from setuptools import find_packages
from setuptools import setup

setup(
    name="covid-map-bc",
    description="Covid map by BC local health area (LHA).",
    author="Mitja Mueller-Jend",
    author_email="mitja.muellerjend@gmail.com",
    version="0.1.0",
    python_requires=">=3.7,<4",
    # Requires are handled by conda.
    install_requires=[],
    include_package_data=True,
    extras_require={
        "develop": ["black", "isort>=4,<5", "pylint"],
    },
    package_data={
        "": ["*.csv", "*.json"],
    },
    packages=find_packages(),
)
