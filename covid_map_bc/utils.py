import csv
import datetime
import math
import os
import time

import pandas

from covid_map_bc.paths import HA_CASE_DETAIL_CACHE
from covid_map_bc.paths import LHA_CASE_DATA_FILE_PATH

os.environ["DISABLE_ARCGIS_LEARN"] = "1"  # isort:skip
from arcgis.mapping import MapServiceLayer  # isort:skip


_EPOCH = datetime.datetime.fromtimestamp(0)
# Remove LHAS merged into northern aggregate.
_SKIP_LHAS = [
    "Snow Country",
    "Stikine",
    "Telegraph Creek",
]
_CASE_TABLE_URL = "https://services1.arcgis.com/xeMpV7tU1t4KD3Ei/arcgis/rest/services/BC_COVID19_Dashboard_Case_Details_Production/FeatureServer/0"


def date_to_ms(date):
    """Return date as 10 char int in milliseconds

    # Folium/Leaflet expects the timestamp to be a string of 10 characters.

    """
    try:
        date = datetime.datetime.strptime(date, "%Y-%m-%d")
    except TypeError:
        return str(int(time.mktime(date.timetuple())))[:10]
    else:
        return str((date - _EPOCH).total_seconds() * 1000)[:10]


def load_case_data_lha(name="LHA_Name"):
    case_data = {}
    keys = []
    dates = []
    index_first_data = 8
    with open(LHA_CASE_DATA_FILE_PATH, "r") as fo:
        case_reader = csv.reader(fo, delimiter=",")
        for line, row in enumerate(case_reader):
            # Determine the number of weeks to process.
            if line == 0:
                keys = row
                dates = keys[index_first_data:]
            else:
                lha_name = row[keys.index("LHA_Name")]
                if lha_name in _SKIP_LHAS:
                    continue
                case_data[lha_name] = {}
                for date in dates:
                    index = keys.index(date)
                    cases = row[index]
                    case_data[lha_name][date] = int(cases) if cases else 0

    return case_data


def download_case_details(progress_bar=None):
    """Download, cache and update case details

    Download from the ArcGIS layer serving the BCCDC Dashboard. Loading all case data
    is pretty slow so a cache is committed as a starting point. This cache is updated
    with new case data when running the app when there is new case data available.
    However, files written on heroku are not persist, so the cache needs to be updated
    periodically to keep performance acceptable when launching the app.

    """
    if progress_bar:
        progress_bar.progress(10)
    layer = MapServiceLayer(_CASE_TABLE_URL)
    if not os.path.isfile(HA_CASE_DETAIL_CACHE):
        case_df = layer.query(as_df=True)
        case_df.to_json(HA_CASE_DETAIL_CACHE, orient="split", indent=2)
    else:
        case_df = pandas.read_json(
            HA_CASE_DETAIL_CACHE, convert_dates=["Reported_Date"], orient="split"
        )
        latest_date = case_df["Reported_Date"].max()
        if progress_bar:
            progress_bar.progress(50)
        new_cases_df = layer.query(
            where="Reported_Date > TIMESTAMP '{}'".format(latest_date), as_df=True
        )
        if progress_bar:
            progress_bar.progress(70)
        if not new_cases_df.empty:
            case_df = pandas.concat([case_df, new_cases_df])
            case_df.drop_duplicates(subset=["ObjectId"], inplace=True)
            case_df.reset_index(inplace=True)
            case_df.to_json(HA_CASE_DETAIL_CACHE, orient="split", indent=2)
            progress_bar.progress(80)
    if progress_bar:
        progress_bar.progress(90)
    return case_df


def interpolate(x0, x1, y0, y1, seek):
    return (y0 * (x1 - seek) + y1 * (seek - x0)) / (x1 - x0)


def clamp(val, minv=0.0, maxv=1.0):
    return max(minv, min(maxv, val))


def get_style_dict(
    geo_data_frame,
    case_data,
    color_gradient,
    max_daily_cases,
    min_opacity=0.3,
    max_opacity=0.7,
    key_name="LHA_Name",
    key_pop="LHA_Pop16",
    div_days=7,
):
    styledict = {}

    for feature in geo_data_frame.iterfeatures():
        name = feature["properties"][key_name]
        index = feature["id"]
        population = feature["properties"][key_pop]
        style_ = {}
        try:
            iterator = case_data[name].iterfeatures()
        except AttributeError:
            iterator = case_data[name].items()
        for date, cases in iterator:
            if math.isnan(cases):
                cases = 0
            case_rate = cases / population * 100000 / div_days
            date = date_to_ms(date)
            style_[date] = {
                "opacity": clamp(
                    interpolate(
                        0, max_daily_cases, min_opacity, max_opacity, case_rate
                    ),
                    maxv=max_opacity,
                ),
                "color": color_gradient(case_rate),
            }
        styledict[index] = style_

    return styledict
