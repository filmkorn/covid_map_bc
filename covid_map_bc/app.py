import datetime
import functools

from branca.colormap import LinearColormap
from branca.colormap import linear
import folium
import geopandas as gpd
import numpy
import pandas
import streamlit as st
from streamlit_folium import folium_static

from covid_map_bc.choropleth import MyTimeSliderChoropleth
from covid_map_bc.paths import HA_GEOJSON_SIMPLE_FILE_PATH
from covid_map_bc.paths import LHA_GEOJSON_SIMPLE_FILE_PATH
from covid_map_bc.utils import download_case_details
from covid_map_bc.utils import get_style_dict
from covid_map_bc.utils import load_case_data_lha

numpy.seterr(divide="ignore", invalid="ignore")
pandas.options.plotting.backend = "plotly"

_TITLE = "British Columbia - COVID-19 map"
_DATA_WEEKLY = "Weekly cases per LHA"
_DATA_DAILY = "Daily cases per HA"
min_opacity = 0.3
max_opacity = 0.7

st.set_page_config(
    page_title=_TITLE,
    layout="wide",
)
sidebar = st.sidebar.title("Navigation")


@st.cache(suppress_st_warning=True)
def _load_case_data(data):
    if data == _DATA_WEEKLY:
        return load_case_data_lha()
    elif data == _DATA_DAILY:
        progress_bar = st.progress(0)  # type:st.progress
        case_details = download_case_details(progress_bar)
        case_details.drop(columns=["Age_Group", "Sex"], inplace=True)
        progress_bar.progress(100)
        progress_bar.empty()
        return case_details


@st.cache()
def _get_dates():
    return [
        datetime.datetime.strptime(date, "%Y-%m-%d")
        for date in next(iter(_load_case_data(data_selected).items()))[1]
    ]


@st.cache
def get_lha_geojson_dataframe():
    return gpd.GeoDataFrame.from_file(LHA_GEOJSON_SIMPLE_FILE_PATH)


@st.cache
def _load_geo_data_frame(file_path):
    return gpd.GeoDataFrame.from_file(file_path)


st.markdown("# " + _TITLE)

data_selected = st.sidebar.selectbox(
    "Data",
    [
        _DATA_WEEKLY,
        _DATA_DAILY,
    ],
)

map_style = st.sidebar.selectbox(
    "Map style",
    [
        "OpenStreetMap",
        "Stamen Terrain",
        "Stamen Toner",
        # "Stamen Watercolor",
        "CartoDB positron",
        # "CartoDB dark_matter",
        # "Mapbox Bright",
        # "Mapbox Control Room",
    ],
    index=3,
)

# Max daily cases per 100k population on the scale.
minv, maxv = (10, 100)

key_name = "LHA_Name" if data_selected == _DATA_WEEKLY else "HA_Name"
key_pop = "LHA_Pop16" if data_selected == _DATA_WEEKLY else "HA_Pop16"

max_daily_cases = st.sidebar.slider(
    "Gradient max:  daily cases / 100k population", minv, maxv, step=10, value=20
)

if data_selected == _DATA_DAILY:
    average_days = st.sidebar.slider("Rolling average: days", 1, 14, value=7)
else:
    lha_detail_type = st.sidebar.selectbox(
        "Case numbers",
        [
            "Table",
            "Line chart",
        ],
    )
    if lha_detail_type == "Table":
        table_shading = st.sidebar.selectbox(
            "Table: conditional formatting",
            [
                "None",
                "Daily cases / 100k population",
                "Relative change",
            ],
        )
    average_days = 1

geo_json_file_path = (
    LHA_GEOJSON_SIMPLE_FILE_PATH
    if data_selected == _DATA_WEEKLY
    else HA_GEOJSON_SIMPLE_FILE_PATH
)


geo_data_frame = _load_geo_data_frame(geo_json_file_path)

case_data = _load_case_data(data_selected)
case_numbers = (
    case_data.groupby(["HA", "Reported_Date"]).size()
    if data_selected == _DATA_DAILY
    else case_data
)

if data_selected == _DATA_DAILY:
    case_df = case_numbers.reset_index()
    case_df = case_df.rename({0: "Case_Count"}, axis=1)
    case_df = case_df.set_index("Reported_Date")
    case_df = case_df.sort_index()
    case_df["Case_Count"] = case_df.groupby(["HA"])["Case_Count"].transform(
        lambda x: x.rolling(
            window=f"{average_days}d",
            # closed="both",
        ).mean()
    )
    case_df = case_df.reset_index()
    case_numbers = case_df.pivot(
        index="Reported_Date", columns="HA", values="Case_Count"
    )


rate_colors = linear.YlOrRd_06.scale(0, max_daily_cases)
rate_colors.caption = (
    "Avarage daily cases per 100k population"
    if data_selected == _DATA_WEEKLY
    else "Daily cases per 100k population"
)

style_dict = get_style_dict(
    geo_data_frame,
    case_numbers,
    rate_colors,
    max_daily_cases,
    key_name=key_name,
    key_pop=key_pop,
    div_days=7 if data_selected == _DATA_WEEKLY else 1,
)


def render_map():
    case_choro = MyTimeSliderChoropleth(
        geo_data_frame,
        name=key_name,
        styledict=style_dict,
        overlay=True,
    )

    lha_choro = folium.Choropleth(
        geo_data_frame,
        name=key_name,
        line_opacity=0.3,
        fill_opacity=0,
        line_weight=1,
        key_on="feature.properties." + key_name,
        highlight=True,
        sticky=True,
    )

    if data_selected == _DATA_WEEKLY:
        fields = ["LHA_Name", "HA_Name", "LHA_Pop16"]
        aliases = ["LHA", "HA", "Population"]
    else:
        fields = ["HA_Name", "HA_Pop16"]
        aliases = ["HA", "Population"]

    lha_choro.geojson.add_child(folium.GeoJsonTooltip(fields=fields, aliases=aliases))

    lha_map = folium.Map(
        location=[53.3014, -127],
        zoom_start=5,
        tiles=map_style,
        prefer_canvas=False,
        zoom_control=True,
    )
    lha_map.add_child(case_choro)
    lha_map.add_child(lha_choro)
    lha_map.add_child(rate_colors)

    folium.plugins.Fullscreen().add_to(lha_map)

    make_map_responsive = """
     <style>
     [title~="st.iframe"] { width: 100%; }
     </style>
    """
    st.markdown(make_map_responsive, unsafe_allow_html=True)
    folium_static(lha_map, height=800)


render_map()


def create_table(case_df):
    if table_shading == "Relative change":
        cases = case_df.to_numpy()
        cases_trailing = cases[:-1]
        cases_leading = cases[1:]

        relative = cases_leading / cases_trailing
        # Replace positive infs from div by zero with 2 - or 100% increase.
        relative = numpy.nan_to_num(relative, nan=1, posinf=4)
        # Replace zero cases with 1 - neutral.
        # relative = numpy.where(relative == 0, 1, relative)

        ones = numpy.ones((1, cases.shape[1]))
        relative = numpy.vstack((ones, relative))

        rate_relative_colors = LinearColormap(
            colors=[(170, 207, 130), "white", (252, 141, 89), (215, 48, 39)],
            index=[0.0, 1.0, 2.0, 4.0],
            vmin=0,
            vmax=4,
            caption="Factor of change from previous week",
        )
        # rate_relative_colors_falling = linear.Greens.scale(1, 0)
        relative_df = pandas.DataFrame(
            data=relative, index=case_df.index, columns=case_df.columns
        )

        case_df = case_df.transpose()
        relative_df = relative_df.transpose()

        def apply_color(x):
            return "background-color: {}".format(rate_relative_colors(x))

        st.dataframe(
            case_df.style.apply(lambda x: relative_df.applymap(apply_color), axis=None)
        )
        st.text(rate_relative_colors.caption)
        # TODO: Find a better way to include the legend.
        st.markdown(rate_relative_colors._repr_html_(), unsafe_allow_html=True)

    elif table_shading == "None":
        case_df = case_df.transpose()
        st.dataframe(case_df)

    elif table_shading == "Daily cases / 100k population":

        @functools.lru_cache()
        def _get_population(lha):
            return geo_data_frame.loc[geo_data_frame["LHA_Name"] == lha]["LHA_Pop16"].max()

        def _get_background_color(weekly_cases: pandas.Series):
            # print(weekly_cases)
            row_style = []
            geo_data_frame.set_index("LHA_Name")
            for lha, cases in weekly_cases.iteritems():
                if cases == 0:
                    row_style.append("")
                    continue
                population = _get_population(lha)
                row_style.append("background-color: {}".format(rate_colors(
                    cases / population * 100000 / 7
                )))
            return row_style

        case_df = case_df.transpose()  # type: pandas.DataFrame
        st.dataframe(
            case_df.style.apply(_get_background_color)
        )
        st.text(rate_colors.caption)
        st.markdown(rate_colors._repr_html_(), unsafe_allow_html=True)


if data_selected == _DATA_WEEKLY:
    st.markdown("## Weekly cases per Local Health Authority (LHA)")

    if lha_detail_type == "Table":
        create_table(pandas.DataFrame.from_dict(case_data))
    elif lha_detail_type == "Line chart":
        case_df = pandas.DataFrame.from_dict(case_data)
        chart = case_df.plot(
            x=case_df.index,
            y=case_df.keys(),
            labels={"value": "Weekly positive cases", "index": "Week ending"},
        )
        st.plotly_chart(chart, use_container_width=True, height=900)

    st.markdown(
        """
### Notes:
- This is **NOT** an official app by the BCCDC  - Case counts are manually transcribed
 from the .PNG maps provided by BCCDC. As this is a tedious process, there may be
 errors in the data.
- __Not all COVID-19 infected individuals are tested and reported; the virus may be
  circulating undetected in the community, including areas where no cases have been
  identified by public health.__
- Cases are mapped by location of residence; cases of unknown residence and
  from out-of-country are not mapped.
- Date shown is the Saturday of each reported week (Sunday to Saturday).
- Local Health Areas (LHA) with higher rates are illustrated with darker shading.
- Shading is based on daily rates per 100.000 population.

### Data sources:
- [BC CDC COVID-19 Data](http://www.bccdc.ca/health-info/diseases-conditions/covid-19/data)
- [COVID Tracking spreadsheet](https://docs.google.com/spreadsheets/d/1omBkX70LYM5byg0eASU-ye5Q0VnRXsZIyRTzED--Fcg) by [/u/crimxona](https://www.reddit.com/user/crimxona/)
- [LHA Boundaries and population (census 2016)](https://catalogue.data.gov.bc.ca/dataset/local-health-area-boundaries)

"""
    )
else:
    case_df.set_index("Reported_Date", inplace=True)
    chart = case_df.plot(
        x=case_df.index,
        y=["Case_Count"],
        color="HA",
        labels={"value": "Cases", "Reported_Date": "Reported Date"},
    )
    st.plotly_chart(chart, use_container_width=True, height=700)

    st.markdown(
        """
### Notes:
- This is **NOT** an official app by the BCCDC.
- Daily case counts are downloaded from the ArcGis Table serving the BCCDC Covid Dashboard.
    
### Data sources:
- [BC CDC COVID-19 Data](http://www.bccdc.ca/health-info/diseases-conditions/covid-19/data)
- [HA Boundaries](https://catalogue.data.gov.bc.ca/dataset/health-authority-boundaries)
"""
    )

st.markdown(
    """### Source code:
[https://gitlab.com/filmkorn/covid_map_bc](https://gitlab.com/filmkorn/covid_map_bc)"""
)

# Clean up some of the larger variables
del case_numbers
del case_data
del geo_data_frame
del style_dict
