import os

_PACKAGE_PATH = os.path.dirname(__file__)
_PROJECT_PATH = os.path.dirname(_PACKAGE_PATH)

try:
    os.mkdir(os.path.join(_PROJECT_PATH, "output"))
except OSError:
    pass

_GENERATED_PATH = os.path.abspath(os.path.join(_PROJECT_PATH, "data", "_generated"))
_CACHE_PATH = os.path.abspath(os.path.join(_PROJECT_PATH, "data", "_cache"))
_LHA_SOURCE_PATH = os.path.abspath(os.path.join(_PROJECT_PATH, "data", "lha_2018"))
LHA_MAP_SHAPE_FILE_PATH = os.path.abspath(
    os.path.join(_LHA_SOURCE_PATH, "LHA_2018.shp")
)

_HA_SOURCE_PATH = os.path.abspath(os.path.join(_PROJECT_PATH, "data", "ha_2018"))
HA_MAP_SHAPE_FILE_PATH = os.path.abspath(os.path.join(_HA_SOURCE_PATH, "HA_2018.shp"))

LHA_GEOJSON_FILE_PATH = os.path.join(_GENERATED_PATH, "LHA_2018.json")
HA_GEOJSON_FILE_PATH = os.path.join(_GENERATED_PATH, "HA_2018.json")
# Simplified map through mapshaper.org
LHA_GEOJSON_SIMPLE_FILE_PATH = os.path.join(
    _GENERATED_PATH, "LHA_2018_simple_05pc.json"
)
HA_GEOJSON_SIMPLE_FILE_PATH = os.path.join(_GENERATED_PATH, "HA_2018_simple_05pc.json")

# Data downloaded from argcis.
HA_CASE_DETAIL_CACHE = os.path.join(_CACHE_PATH, "BC_HA_Case_details.json")

LHA_CASE_DATA_FILE_PATH = os.path.join(
    _GENERATED_PATH, "COVID-19_BC_LHA_weekly_cases.csv"
)
