# British Columbia Local Health Authority - COVID-19 weekly case map

Interactive map of the Local Health Authority case rate maps.

 ![](streamlit_app.webm)

## Notes:

- Cases counts are manually transcribed from the PNG maps provided by BCCDC. As this is a tedious process, there may be
errors in the data.

## Getting started

- Install Conda
- Install Python-3.7+

```
conda env create --file=environment.yml
pip install -e .[develop]

streamlit run covid_map_bc/app.py
```

Update conda packages from `environment.yml`:
```
conda env update --file=environment.yml
```

To format the code:

```
tox -e format_code
```
